//3. Class: CheckAge
public class CheckAge{
	public static void main(String[]args){
		int age = 19;
		String criteria;
		if(age < 10) { 
			criteria = "Kids";
		} else if(age < 20) { 
			criteria = "Teen";
		} else if(age < 50) { 
			criteria = "Adult";
		} else {
			criteria = "Senior";
		}
		System.out.println(criteria);
	}
}