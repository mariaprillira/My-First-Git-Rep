import java.util.Scanner;
public class SistemSensus {

	public static void main(String[] args) {
		// Create a new Scanner object to read input from standard input
		Scanner input = new Scanner(System.in);
		// The interface to get input from user
		System.out.print("CENSUS DATA PRINTING PROGRAM\n" +
				"--------------------\n" +
				"Name of Family Head   : ");
		String name = input.nextLine();
		System.out.print("Home Address           : ");
		String address = input.nextLine();
		System.out.print("Body Length (cm)     : ");
		int length = Integer.parseInt(input.nextLine());
		System.out.print("Body Width (cm)       : ");
		int width = Integer.parseInt(input.nextLine());
		System.out.print("Body Height (cm)      : ");
		int height = Integer.parseInt(input.nextLine());
		System.out.print("Body Weight (kg)       : ");
		int weight = Integer.parseInt(input.nextLine());
		System.out.print("Number of Family Members: ");
		int members = Integer.parseInt(input.nextLine());
		System.out.print("Date of Birth          : ");
		String birthDate = input.nextLine();
		System.out.print("Additional Notes       : ");
		String notes = input.nextLine();
		System.out.print("Number of Data Prints    : ");
        int numberPrints = Integer.parseInt(input.nextLine());
        
		int ratio = (weight)/((length/100)*(height/100)*(width/100));

		for (int a=1; a<=numberPrints; a++) {
            // print result
			System.out.print("Printing " + a + " of " + numberPrints + " for: ");
			String receiver = input.nextLine().toUpperCase(); // Read input and transform input to upper
            System.out.print("DATA PREPARED FOR " + receiver +
            "--------------------\n");
            System.out.print(name + "-" + address);
            System.out.print("Born on " + birthDate);
            System.out.print( "Ratio Weight Per Volume = " + ratio + "kg / m ^ 3");
			int noteLength = notes.length();
			if (noteLength == 0){ 
				String note = "No additional notes";
				System.out.print("Note: " + note);
			}
			else{
				System.out.print("Note: " + notes);
			}
		}
		input.close();
	}
}
