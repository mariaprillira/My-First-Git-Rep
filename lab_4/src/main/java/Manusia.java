public class Manusia{
    //variable Human
    private String nama;
    private int umur;
    private int uang;
    private double kebahagiaan;

    // constructur for class Human
    public Manusia (String nama, int umur, int uang, double kebahagiaan){
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.kebahagiaan = kebahagiaan;
    }

    public Manusia(String nama, int umur){
        this(nama, umur, 50000, 50);
    }

    public Manusia(String nama, int umur, int uang){
        this(nama, umur, uang, 50);
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getNama(){
        return nama;
    }

    public void setUmur (int umur){
        this.umur = umur;
    }

    public int getUmur(){
        return umur;
    }

    public void setUang(int uang){
        this.uang = uang;
    }

    public int getUang(){
        return uang;
    }

    public void setKebahagiaan(double kebahagiaan){
        if (kebahagiaan <= 0){
            this.kebahagiaan = 0;
        } else if (kebahagiaan >= 100){
            this.kebahagiaan = 100;
        }
        else {
			this.kebahagiaan = kebahagiaan;
		}
    }

    public double getKebahagiaan(){
        return kebahagiaan;
    }

    public void beriUang(Manusia penerima){
        int ascii = 0;
        for (int i=0; i<penerima.nama.length();i++){
            char newnama = penerima.nama.charAt(i);
            int angka = (int)newnama;
            ascii += angka;
        }

        int jumlahUang = ascii * 100;
        if (jumlahUang < this.uang){
            this.uang -= jumlahUang;
            penerima.uang += jumlahUang;

            double newKebahagiaan = this.getKebahagiaan() + (double)jumlahUang/6000;
            this.setKebahagiaan(newKebahagiaan);
            newKebahagiaan = penerima.getKebahagiaan() + (double)jumlahUang/6000;
            penerima.setKebahagiaan(newKebahagiaan);
            
            if (newKebahagiaan > 100){
                newKebahagiaan = 100;
                this.setKebahagiaan(newKebahagiaan);
            }
            
            System.out.println(this.getNama()+ " memeberi uang sebesar "+ jumlahUang+" kepada " + penerima.getNama()+" mereka berdua senang :D");
        }else {
            System.out.println(this.getNama()+" ingin memberi uang kepada " + penerima.getNama()+" namun tidak memiliki cukup uang :'(:'(");
        }
    }

    public void sakit(String namaPenyakit){
        double newKebahagiaan = this.getKebahagiaan() - (double)namaPenyakit.length();
        this.setKebahagiaan(newKebahagiaan);
        
        if (newKebahagiaan < 0){
            newKebahagiaan = 0;
            this.setKebahagiaan(newKebahagiaan);
        }
        System.out.println(this.getNama()+" terkena penyakit "+ namaPenyakit+" :O0");
    }

    public void bekerja (int durasi, int bebanKerja){
        if (this.umur < 18){
            System.out.println(this.nama +" belum boleh bekerja karena masih dibawah umur D:");
        }else if (this.umur>=18){
            int BebanKerjaTotal = durasi * bebanKerja;
            int Pendapatan = 0;
            if (BebanKerjaTotal<=this.kebahagiaan){
                Pendapatan += BebanKerjaTotal * 10000;
                double newKebahagiaan = this.getKebahagiaan() - (double)BebanKerjaTotal;
                this.setKebahagiaan(newKebahagiaan);
                System.out.println(this.nama+" bekerja fulltime, total pendapatan: "+ Pendapatan);
            }else if (BebanKerjaTotal>this.kebahagiaan){
                double durasibaru = this.getKebahagiaan()/(double)bebanKerja;
                int Durasi_baru = (int) durasibaru;
                BebanKerjaTotal = Durasi_baru * bebanKerja;
                double newKebahagiaan = this.getKebahagiaan()-(float) BebanKerjaTotal;
                this.setKebahagiaan(newKebahagiaan);
                Pendapatan += BebanKerjaTotal * 10000;
                System.out.println(this.nama+" tidak bekerja fulltime sudah lelah, total pendapatan: "+ Pendapatan);
            }
            this.uang +=Pendapatan;
        }

    }

    public void beriUang(Manusia penerima, int jumlah_uang){
        if (jumlah_uang < this.uang){
            this.uang -= jumlah_uang;
            penerima.uang += jumlah_uang;
            double newKebahagiaan = this.getKebahagiaan() + (double)jumlah_uang/6000;
            this.setKebahagiaan(newKebahagiaan);
            newKebahagiaan = penerima.getKebahagiaan() + (double)jumlah_uang/6000;
            penerima.setKebahagiaan(newKebahagiaan);
            
            if (newKebahagiaan>100){
                newKebahagiaan = 100;
                this.setKebahagiaan(newKebahagiaan);
            }
            System.out.println(this.nama + " memberi uang sebanyak "+ jumlah_uang +" kepada "+penerima.nama+" mereka berdua senang :D! yay");
        }else{
            System.out.println(this.nama+ " ingin memberi uang kepada " + penerima.nama +" namun tidak memiliki cukup uang D':");
        }
    }

    public void rekreasi(String tempat){
        int biaya = tempat.length()*10000;
        if (this.uang > biaya){
            this.uang -= biaya;
            double newKebahagiaan = this.getKebahagiaan() + (double)tempat.length();
            this.setKebahagiaan(newKebahagiaan);
            System.out.println(this.getNama()+" berekreasi di "+tempat+ this.getNama()+" senang:))");
        }else{
            System.out.println(this.getNama()+" tidak mempunyai cukup uang untuk berekreasi di"+ tempat+" :(((");
        }
    }

    public String toString(){
        return "Nama\t\t"+this.nama+"\n"+
               "Umur\t\t"+this.umur+"\n"+
               "Uang\t\t"+this.uang+"\n"+
               "Kebahagiaan\t"+this.kebahagiaan;

    }
}
