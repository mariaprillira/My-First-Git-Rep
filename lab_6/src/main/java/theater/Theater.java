package theater;
import ticket.Ticket;
import movie.Movie;
import java.util.ArrayList;

public class Theater{
    private String theater_name;
    private int balance;
    private ArrayList<Ticket> listticket;
    private Movie[] listmovie;

    public Theater (String theater_name, int balance, ArrayList<Ticket> listticket, Movie[] listmovie){
        this.theater_name = theater_name;
        this.balance = balance;
        this.listticket = listticket;
        this.listmovie = listmovie;
    }

    public int getBalance(){
        return balance;
    }
    public String getName(){
        return theater_name;
    }
    public ArrayList<Ticket> getTicket(){
        return listticket;
    }
    
    public static void printTotalRevenueEarned(Theater[] theaters){
        int totalamount = 0;
        for (Theater theater: theaters){
            totalamount += theater.getBalance();
        }
        System.out.println("Koh Mas's Total Revenue:" + totalamount );
        System.out.println("----------------------------------------------------------");
        for (Theater theater: theaters){
            System.out.println("\nCinema\t\t :" + theater.getName()+ "\n"+
                                "Balance\t\t :" + theater.getBalance() + "\n");
        }
        System.out.println("----------------------------------------------------------");  
    }

    public void printInfo(){
        System.out.println("----------------------------------------------------------");
        System.out.print("Cinema\t\t\t\t: " +  theater_name + "\n"+
            "Cash Balance\t\t\t: " + balance + "\n" +
            "Number of tickets available\t: " + listticket.size()+ "\n"+
            "Movie list available\t\t: ");
        for (int i=0; i < listmovie.length; i++){
            System.out.print(listmovie[i].get_title());
            if(i != listmovie.length-1) {
                System.out.print(", ");
            } else {
                System.out.println();
            }
        }
        System.out.println("----------------------------------------------------------");
    }

}