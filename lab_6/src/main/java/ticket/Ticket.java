package ticket;
import java.util.ArrayList;
import movie.Movie;
public class Ticket{
    private int price = 60000;
    private String showday, type;
    private Movie film;

    public String getShowday(){
        return showday;
    }
    public Movie getMovie(){
        return film;
    }
    public String getType(){
        return type;
    }
    public int getprice(){
        return price; 
    }

    public Ticket(Movie film, String showday, boolean type){
        this.showday = showday;
        this.type = (type) ? "3 Dimensi" : "Biasa";
        this.film = film;
        this.price = (showday.equals("Minggu")||showday.equals("Sabtu"))? 100000 : 60000;
        this.price = (type)? (int)(this.price*1.2) : this.price;
    }

    public void printInfo(){
        System.out.println("----------------------------------------------------------");
        System.out.println("Movie:\t" + film.get_title() + "\n"+
                "Showday:\t" + this.showday + "\n"+
                "Type"+this.type+"\n");
        System.out.println("----------------------------------------------------------");
    }
}