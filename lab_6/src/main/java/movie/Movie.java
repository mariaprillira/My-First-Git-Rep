package movie;
public class Movie{
    private String title;
    private String age_rating;
    private String genre;
    private int duration;
    private String status;

    //constructur
    public Movie (String title, String age_rating,  int duration, String genre, String status){
        this.title = title;
        this.age_rating = age_rating;
        this.genre = genre;
        this.duration = duration;
        this.status = status;
    }
    
    public String get_title(){
        return title;
    }

    public String getrating(){
        return age_rating;
    }

    public void printInfo(){
        System.out.println("----------------------------------------------------------");
        System.out.println("Title\t : " + this.title + "\n"+
                "Genre\t : " + this.genre + "\n"+
                "Duration\t : "+ this.duration +"\n"+
                "Rating\t : "+ this.age_rating + "\n" +
                "Type : "+ this.status + "\n");
        System.out.println("----------------------------------------------------------");
    }
}