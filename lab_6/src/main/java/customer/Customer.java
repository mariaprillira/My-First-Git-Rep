package customer;
import java.util.ArrayList;
import ticket.Ticket;
import movie.Movie;
import theater.Theater;


public class Customer{
    private String nama;
    private String gender;
    private int age;

    public Customer(String nama, String gender, int age){
        this.nama = nama;
        this.gender = gender;
        this.age = age;
    }

    public Ticket orderTicket(Theater theater, String title, String day, String type){
        // for every film in movie list 
        for (int i = 0; i< theater.getTicket().size(); i++){
            Ticket ticket = theater.getTicket().get(i);
            if (ticket.getMovie().get_title().equals("title")){
                if (ticket.getShowday().equals(day)){
                    if (ticket.getMovie().getrating().equals("Remaja") && this.age <13 || ticket.getMovie().getrating().equals("Dewasa")&& this.age<17){             
                        System.out.println(String.format("%s is not old enought to watch %s.", this.nama, ticket.getMovie()));
                    }
                    else{
                        System.out.println(String.format("%s has purchased ticket for %s type %s on %s for Rp. %d", nama, title, type, day, ticket.getprice()));
                    }
                }      
            }
        }
        System.out.println(String.format("Tickets for the movie %s type %s on %s are not available in %s", title, type, day, theater.getName()));
        return null;
    }

    public void findMovie(Theater theater, String title){
        for (int i = 0; i< theater.getTicket().size(); i++){
            Ticket ticket = theater.getTicket().get(i);
            if(ticket.getMovie().get_title().equals("title")){
                ticket.getMovie().printInfo();
                return;
            }
        }
        System.out.println(String.format("Movie %s searched by %s is not in the cinema %s", title, this.nama, theater.getName()));
    }
}