import java.util.Scanner;

public class StartBingo{
    
    public static void main(String[]args){
        System.out.println("Start Playing Bingo!! Please input the Numbers!!");
        // input array list
        Number[][] ary1 = new Number[5][5];
        Number[] ary2 = new Number[100];

        Scanner s = new Scanner(System.in);

        //input numbers to arraylist 
        for (int i=0; i<5; i++){
            String input = s.nextLine();
            String[] splitinput = input.split(" ");
            for (int j=0; j<5; j++){
                ary1[i][j] = new Number(Integer.parseInt(splitinput[j]), i, j);
                ary2[Integer.parseInt(splitinput[j])] = ary1[i][j];
            }
        }
    

        BingoCard startgame = new BingoCard(ary1, ary2);

        while(true){
            //new input 
            String newinput = s.nextLine().toUpperCase();
            String[] newinputsplit = newinput.split(" ");

            String input_ = newinputsplit[0];

            //to run the method MARK

            if (input_.equals("MARK")){
                System.out.println(startgame.markNum(Integer.parseInt(newinputsplit[1])));
                if (startgame.isBingo()==true){
                    System.out.println("BINGO!");
                    System.out.println(startgame.info());
                    break;
                } 
            }
            //to run the method info
            else if (input_.equals("INFO")){
                System.out.println(startgame.info());
            }
            //to run the method restart
            else if (input_.equals("RESTART")){
                System.out.println(startgame.restart());
            }
        }
    }
}